package com.ddpt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class PtRegistryApplication {
    public static void main(String[] args) {
        SpringApplication.run(PtRegistryApplication.class,args);
    }
}
