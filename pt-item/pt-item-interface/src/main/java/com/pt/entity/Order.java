package com.pt.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "t_order")
public class Order {

    @Id
    private Integer id;

    private String title;

    private String content;

    @Column(name = "p_name")
    private String pName;

    @Column(name = "c_name")
    private String cName;

    private Integer cost;

    @Column(name = "a_statu")
    private Integer aStatu;

    @Column(name = "o_statu")
    private Integer oStatu;

    @Column(name = "e_content")
    private String eContent;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date evtime;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date start;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date end;

    @Column(name = "c_id")
    private Integer cId;

    @Column(name = "p_id")
    private Integer pId;

    private String address;

    private String people;

    private String contact;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Integer getaStatu() {
        return aStatu;
    }

    public void setaStatu(Integer aStatu) {
        this.aStatu = aStatu;
    }

    public Integer getoStatu() {
        return oStatu;
    }

    public void setoStatu(Integer oStatu) {
        this.oStatu = oStatu;
    }

    public String geteContent() {
        return eContent;
    }

    public void seteContent(String eContent) {
        this.eContent = eContent;
    }

    public Date getEvtime() {
        return evtime;
    }

    public void setEvtime(Date evtime) {
        this.evtime = evtime;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Integer getcId() {
        return cId;
    }

    public void setcId(Integer cId) {
        this.cId = cId;
    }

    public Integer getpId() {
        return pId;
    }

    public void setpId(Integer pId) {
        this.pId = pId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPeople() {
        return people;
    }

    public void setPeople(String people) {
        this.people = people;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
